<?php

/**
 * @file
 * Provide views data for sug.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function sug_views_data_alter(&$data) {

  $data['user_group_roles']['table']['group']  = t('User');

  $data['user_group_roles']['table']['join'] = array(
    'users_field_data' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['user_group_roles']['rid'] = array(
    'title' => t('Group roles'),
    'help' => t('Group roles that a user belongs to.'),
    'field' => array(
      'id' => 'user_group_roles',
      'no group by' => TRUE,
    ),
    'filter' => array(
      'id' => 'user_group_roles',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'id' => 'user_group_roles_rid',
      'name table' => 'group_roles',
      'name field' => 'name',
      'empty field name' => t('No group role'),
      'zero is null' => TRUE,
      'numeric' => TRUE,
    ),
  );



}
