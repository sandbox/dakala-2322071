/**
 * @file
 * Javascript for the node content editing form.
 */

(function ($) {

  'use strict';

  /**
   * Behaviors for setting summaries on content type form.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behaviors on content type edit forms.
   */
  Drupal.behaviors.sugContentTypes = {
    attach: function (context) {
      var $context = $(context);
      // Provide the vertical tab summaries.
      $context.find('#edit-groups').drupalSetSummary(function (context) {
        var vals = [];
        var $editContext = $(context);

        $editContext.find('input:checked').next('label').each(function () {
          vals.push(Drupal.checkPlain($(this).text()));
        });

        return vals.join(', ');
      });
    }
  };

})(jQuery);
