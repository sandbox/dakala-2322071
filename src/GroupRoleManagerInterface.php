<?php
/**
 * Created by PhpStorm.
 * User: dakala
 * Date: 16/12/2015
 * Time: 09:36
 */

namespace Drupal\sug;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;

interface GroupRoleManagerInterface {

  public function getGroupRoles($exclude_locked_roles = FALSE);

  public function hasGroupRole($rid);

  public function hasGroupPermission($entity, AccountInterface $account, $permission = '');
}
