<?php


namespace Drupal\sug\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\sug\GroupManagerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\VocabularyStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Xss;
use Drupal\taxonomy\Entity\Term;

class GroupController extends ControllerBase {

  /**
   * Group manager service.
   *
   * @var \Drupal\sug\GroupManagerInterface
   */
  protected $groupManager;

  /**
   * Vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Node access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $nodeAccess;

  /**
   * Field map of existing fields on the site.
   *
   * @var array
   */
  protected $fieldMap;

  /**
   * Node type storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Node entity type, we need to get cache tags from here.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $nodeEntityTypeDefinition;


  /**
   * Constructs a ForumController object.
   *
   * @param \Drupal\sug\GroupManagerInterface $group_manager
   *   The group manager service.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   Vocabulary storage.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   Term storage.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current logged in user.
   * @param \Drupal\Core\Entity\EntityAccessControlHandlerInterface $node_access
   *   Node access control handler.
   * @param array $field_map
   *   Array of active fields on the site.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_type_storage
   *   Node type storage handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityTypeInterface $node_entity_type_definition
   *   Node entity type definition object
   */
  public function __construct(GroupManagerInterface $group_manager, VocabularyStorageInterface $vocabulary_storage, TermStorageInterface $term_storage, AccountInterface $current_user, EntityAccessControlHandlerInterface $node_access, array $field_map, EntityStorageInterface $node_type_storage, RendererInterface $renderer, EntityTypeInterface $node_entity_type_definition) {
    $this->groupManager = $group_manager;
    $this->vocabularyStorage = $vocabulary_storage;
    $this->termStorage = $term_storage;
    $this->currentUser = $current_user;
    $this->nodeAccess = $node_access;
    $this->fieldMap = $field_map;
    $this->nodeTypeStorage = $node_type_storage;
    $this->renderer = $renderer;
    $this->nodeEntityTypeDefinition = $node_entity_type_definition;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityManagerInterface $entity_manager */
    $entity_manager = $container->get('entity.manager');

    return new static(
      $container->get('group.manager'),
      $entity_manager->getStorage('taxonomy_vocabulary'),
      $entity_manager->getStorage('taxonomy_term'),
      $container->get('current_user'),
      $entity_manager->getAccessControlHandler('node'),
      $entity_manager->getFieldMap(),
      $entity_manager->getStorage('node_type'),
      $container->get('renderer'),
      $entity_manager->getDefinition('node')
    );
  }

  /**
   * Returns add group entity form.
   *
   * @return array
   *   Render array for the add form.
   */
  public function addGroup() {
    $vid = $this->config('sug.settings')->get('vocabulary');
    $taxonomy_term = $this->termStorage->create(array(
      'vid' => $vid,
      'group_container' => 0,
    ));
    return $this->entityFormBuilder()->getForm($taxonomy_term, 'group');
  }

  /**
   * Returns add container entity form.
   *
   * @return array
   *   Render array for the add form.
   */
  public function addContainer() {
    $vid = $this->config('sug.settings')->get('vocabulary');
    $taxonomy_term = $this->termStorage->create(array(
      'vid' => $vid,
      'group_container' => 1,
    ));
    return $this->entityFormBuilder()->getForm($taxonomy_term, 'group-container');
  }



  /**
   * Returns group page for a given group.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *   The group to render the page for.
   *
   * @return array
   *   A render array.
   */
  public function groupPage(TermInterface $taxonomy_term) {
    return views_embed_view('group_homepage', 'embed_1', $taxonomy_term->id());
  }

  public function termTitle(Term $taxonomy_term) {
    $term = (!$taxonomy_term instanceof Term) ? Term::load($taxonomy_term) : $taxonomy_term;
    return Xss::filter($term->label());
  }

  public function groupMembersList($taxonomy_term) {
    return views_embed_view('group_members_list', 'embed_1', $taxonomy_term);
  }

  public function groupMembersAdd($taxonomy_term) {
    return views_embed_view('group_members_list', 'embed_2', $taxonomy_term);
  }

  /**
   * TODO: This should only be containers or configurable for large organisations.
   * Smaller sites can afford to list all groups on a single page.
   *
   * Returns group index page.
   *
   * @return array
   *   A render array.
   */
  public function groupIndex() {
    $vocabulary = $this->vocabularyStorage->load($this->config('sug.settings')->get('vocabulary'));
    $index = $this->groupManager->getIndex();
    $build = $this->build($index->groups, $index);
    if (empty($index->groups)) {
      // Root of empty group.
      $build['#title'] = $this->t('No groups defined');
    }
    else {
      // Set the page title to group's vocabulary name.
      $build['#title'] = $vocabulary->label();
      $this->renderer->addCacheableDependency($build, $vocabulary);
    }
    return $build;
  }


  /**
   * Returns a renderable group index page array.
   *
   * @param array $groups
   *   A list of groups.
   * @param \Drupal\taxonomy\TermInterface $term
   *   The taxonomy term of the group.
   * @param array $topics
   *   The topics of this group.
   * @param array $parents
   *   The parent groups in relation this group.
   * @param array $header
   *   Array of header cells.
   *
   * @return array
   *   A render array.
   */
  protected function build($groups, TermInterface $term, $topics = array(), $parents = array(), $header = array()) {
    $config = $this->config('sug.settings');
    $build = array(
      '#theme' => 'groups',
      '#groups' => $groups,
      '#topics' => $topics,
      '#parents' => $parents,
      '#header' => $header,
      '#term' => $term,
      '#sortby' => $config->get('topics.order'),
      '#groups_per_page' => $config->get('topics.page_limit'),
    );
    if (empty($term->group_container->value)) {
      $build['#attached']['feed'][] = array('taxonomy/term/' . $term->id() . '/feed', 'RSS - ' . $term->getName());
    }
    $this->renderer->addCacheableDependency($build, $config);

    foreach ($groups as $group) {
      $this->renderer->addCacheableDependency($build, $group);
    }
    foreach ($topics as $topic) {
      $this->renderer->addCacheableDependency($build, $topic);
    }
    foreach ($parents as $parent) {
      $this->renderer->addCacheableDependency($build, $parent);
    }
    $this->renderer->addCacheableDependency($build, $term);

    return [
      'group' => $build,
      '#cache' => [
        'tags' => Cache::mergeTags($this->nodeEntityTypeDefinition->getListCacheTags()),
      ],
    ];
  }

  public function settingsPlaceholder() {
    return $build['placeholder'] = array(
      '#prefix' => '<p>',
      '#markup' => __CLASS__ . '::' . __FUNCTION__ . '()',
      '#suffix' => '</p>',
    );
  }

}