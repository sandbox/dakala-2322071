<?php

/**
 * @file
 * Contains \Drupal\group\GroupManager.
 */

namespace Drupal\sug;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides group manager service.
 */
class GroupManager implements GroupManagerInterface {
  use StringTranslationTrait;
  use DependencySerializationTrait {
    __wakeup as defaultWakeup;
    __sleep as defaultSleep;
  }

  /**
   * Group sort order, newest first.
   */
  const NEWEST_FIRST = 1;

  /**
   * Group sort order, oldest first.
   */
  const OLDEST_FIRST = 2;

  /**
   * Group settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity manager service
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Database connection
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Array of last post information keyed by group (term) id.
   *
   * @var array
   */
  protected $lastPostData = array();

  /**
   * Array of group statistics keyed by group (term) id.
   *
   * @var array
   */
  protected $groupStatistics = array();

  /**
   * Array of group children keyed by parent group (term) id.
   *
   * @var array
   */
  protected $groupChildren = array();

  /**
   * Array of history keyed by nid.
   *
   * @var array
   */
  protected $history = array();

  /**
   * Cached group index.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $index;


  /**
   * Constructs the group manager service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityManagerInterface $entity_manager, Connection $connection, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->entityManager = $entity_manager;
    $this->connection = $connection;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getTopics($tid, AccountInterface $account) {
    $config = $this->configFactory->get('sug.settings');
    $group_per_page = $config->get('topics.page_limit');
    $sortby = $config->get('topics.order');

    $header = array(
      array('data' => $this->t('Posts'), 'field' => 'f.title'),
      array('data' => $this->t('Type'), 'field' => 'f.title'),
      array('data' => $this->t('Author'), 'field' => 'f.title'),
      array('data' => $this->t('Created at'), 'field' => 'f.created'),
    );

    $order = $this->getTopicOrder($sortby);
    for ($i = 0; $i < count($header); $i++) {
      if ($header[$i]['field'] == $order['field']) {
        $header[$i]['sort'] = $order['sort'];
      }
    }

    $query = $this->connection->select('groups_index', 'f')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('Drupal\Core\Database\Query\TableSortExtender');
    $query->fields('f');
    $query
      ->condition('f.tid', $tid)
      ->addTag('node_access')
      ->addMetaData('base_table', 'groups_index')
      ->orderBy('f.sticky', 'DESC')
      ->orderByHeader($header)
      ->limit($group_per_page);

    $count_query = $this->connection->select('groups_index', 'f');
    $count_query->condition('f.tid', $tid);
    $count_query->addExpression('COUNT(*)');
    $count_query->addTag('node_access');
    $count_query->addMetaData('base_table', 'groups_index');

    $query->setCountQuery($count_query);
    $result = $query->execute();
    $nids = array();
    foreach ($result as $record) {
      $nids[] = $record->nid;
    }
    if ($nids) {
      $nodes = $this->entityManager->getStorage('node')->loadMultiple($nids);

      $query = $this->connection->select('node_field_data', 'n')
        ->extend('Drupal\Core\Database\Query\TableSortExtender');
      $query->fields('n', array('nid'));

      $query->join('groups_index', 'f', 'f.nid = n.nid');
      $query->addField('f', 'tid', 'group_tid');

      $query->join('users_field_data', 'u', 'n.uid = u.uid AND u.default_langcode = 1');
      $query->addField('u', 'name');

      $query
        ->orderBy('f.sticky', 'DESC')
        ->orderByHeader($header)
        ->condition('n.nid', $nids, 'IN')
        // @todo This should be actually filtering on the desired node language
        //   and just fall back to the default language.
        ->condition('n.default_langcode', 1);

      $result = array();
      foreach ($query->execute() as $row) {
        $topic = $nodes[$row->nid];

        foreach ($row as $key => $value) {
          $topic->{$key} = $value;
        }
        $result[] = $topic;
      }
    }
    else {
      $result = array();
    }

    $topics = array();
    $first_new_found = FALSE;
    foreach ($result as $topic) {
      // author, created_at

      $topics[$topic->id()] = $topic;
    }

    return array('topics' => $topics, 'header' => $header);

  }

  /**
   * TODO: debug SQL statements.
   *
   * Gets topic sorting information based on an integer code.
   *
   * @param int $sortby
   *   One of the following integers indicating the sort criteria:
   *   - GroupManager::NEWEST_FIRST: Date - newest first.
   *   - GroupManager::OLDEST_FIRST: Date - oldest first.
   *
   * @return array
   *   An array with the following values:
   *   - field: A field for an SQL query.
   *   - sort: 'asc' or 'desc'.
   */
  protected function getTopicOrder($sortby) {
    switch ($sortby) {
      case static::NEWEST_FIRST:
        return array('field' => 'f.created', 'sort' => 'desc');

      case static::OLDEST_FIRST:
        return array('field' => 'f.created', 'sort' => 'asc');
    }
  }

  /**
   * Gets the last time the user viewed a node.
   *
   * @param int $nid
   *   The node ID.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to fetch last time for.
   *
   * @return int
   *   The timestamp when the user last viewed this node, if the user has
   *   previously viewed the node; otherwise HISTORY_READ_LIMIT.
   */
  protected function lastVisit($nid, AccountInterface $account) {
    if (empty($this->history[$nid])) {
      $result = $this->connection->select('history', 'h')
        ->fields('h', array('nid', 'timestamp'))
        ->condition('uid', $account->id())
        ->execute();
      foreach ($result as $t) {
        $this->history[$t->nid] = $t->timestamp > HISTORY_READ_LIMIT ? $t->timestamp : HISTORY_READ_LIMIT;
      }
    }
    return isset($this->history[$nid]) ? $this->history[$nid] : HISTORY_READ_LIMIT;
  }

  /**
   * TODO: debug SQL query.
   *
   * Provides the last post information for the given group tid.
   *
   * @param int $tid
   *   The group tid.
   *
   * @return \stdClass
   *   The last post for the given group.
   */
  protected function getLastPost($tid) {
    if (!empty($this->lastPostData[$tid])) {
      return $this->lastPostData[$tid];
    }
    // Query "Last Post" information for this group.
    $query = $this->connection->select('node_field_data', 'n');
    $query->join('groups', 'g', 'n.vid = g.vid AND g.tid = :tid', array(':tid' => $tid));
    $query->join('users_field_data', 'u', 'n.uid = u.uid AND u.default_langcode = 1');

    $topic = $query
      ->fields('u', array('name', 'uid'))
      ->fields('n', array('created'))
      ->condition('n.status', 1)
      ->orderBy('created', 'DESC')
      ->range(0, 1)
      ->addTag('node_access')
      ->execute()
      ->fetchObject();

    // Build the last post information.
    $last_post = new \stdClass();
    if (!empty($topic->created)) {
      $last_post->created = $topic->created;
      $last_post->name = $topic->name;
      $last_post->uid = $topic->uid;
    }

    $this->lastPostData[$tid] = $last_post;
    return $last_post;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren($vid, $tid) {
    if (!empty($this->groupChildren[$tid])) {
      return $this->groupChildren[$tid];
    }
    $groups = array();
    $_groups = $this->entityManager->getStorage('taxonomy_term')->loadTree($vid, $tid, NULL, TRUE);
    foreach ($_groups as $group) {
      // Merge in the user and post counters.
      $group->num_members = ($count = $this->userCount($group->id())) ? $count : 0;
      $group->num_posts = ($count = $this->postCount($group->id())) ? $count : 0;

      // Merge in last post details.
      $group->last_post = $this->getLastPost($group->id());
      $groups[$group->id()] = $group;
    }

    $this->groupChildren[$tid] = $groups;
    return $groups;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndex() {
    if ($this->index) {
      return $this->index;
    }

    $vid = $this->configFactory->get('sug.settings')->get('vocabulary');
    $index = $this->entityManager->getStorage('taxonomy_term')->create(array(
      'tid' => 0,
      'container' => 1,
      'parents' => array(),
      'isIndex' => TRUE,
      'vid' => $vid
    ));

    // Load the tree below.
    $index->groups = $this->getChildren($vid, 0);
    $this->index = $index;
    return $index;
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache() {
    // Reset the index.
    $this->index = NULL;
    // Reset history.
    $this->history = array();
  }

  /**
   * {@inheritdoc}
   */
  public function getParents($tid) {
    return $this->entityManager->getStorage('taxonomy_term')->loadAllParents($tid);
  }

  /**
   * {@inheritdoc}
   */
  public function checkNodeType(EntityInterface $entity) {
    return ($entity instanceof FieldableEntityInterface) ? $this->isGroupNodeType($entity->getEntityType()->id(), $entity->bundle()) : FALSE;
  }

  /**
   * @todo: add to interface?
   *
   * @param $type
   * @return bool
   */
  public function isGroupNodeType($type, $bundle) {
    // @todo: Fixme
    // Fetch information about the group field.
    $field_definitions = $this->entityManager->getFieldDefinitions($type, $bundle);
    return !empty($field_definitions['taxonomy_groups']);
  }

  /**
   * {@inheritdoc}
   */
  public function unreadTopics($term, $uid) {
    $query = $this->connection->select('node_field_data', 'n');
    $query->join('groups', 'g', 'n.vid = g.vid AND g.tid = :tid', array(':tid' => $term));
    $query->leftJoin('history', 'h', 'n.nid = h.nid AND h.uid = :uid', array(':uid' => $uid));
    $query->addExpression('COUNT(n.nid)', 'count');
    return $query
      ->condition('status', 1)
      // @todo This should be actually filtering on the desired node status
      //   field language and just fall back to the default language.
      ->condition('n.default_langcode', 1)
      ->condition('n.created', HISTORY_READ_LIMIT, '>')
      ->isNull('h.nid')
      ->addTag('node_access')
      ->execute()
      ->fetchField();
  }

  /**
   * Get the label for a group with taxonomy terms.
   *
   * @param $term
   * @return string
   */
  public function getGroupTagLabel($term) {
    $label = $term->getName();
    $labels = $this->getGroupTags($term);
    if (count($labels)) {
      $label .= sprintf(' (%s)', implode(', ', $labels));
    }
    return $label;
  }

  /**
   * Get all term tags for a group with taxonomy terms.
   *
   * @param $term
   * @return array
   */
  public function getGroupTags($term) {
    $labels = array();
    $node_types = array_keys(array_filter(\Drupal::config('sug.settings')->get('group_entity')));
    if (count($node_types)) {
      foreach ($node_types as $node_type) {
        if (!empty($nid = $term->{'group_' . $node_type}->target_id)) {
          $node = Node::load($nid);
          $labels[] = $node->label();
        }
      }
    }

    $vids = \Drupal::config('sug.settings')->get('group_vocabulary');
    if (count($vids)) {
      foreach ($vids as $vid) {
        if (!empty($tid = $term->{'group_' . $vid}->target_id)) {
          $vocabulary = Vocabulary::load($vid);
          $tag = Term::load($tid);
          $labels[] = $vocabulary->label() . ': ' . $tag->getName();
        }
      }
    }
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroups($entity) {
    $groups = array();
    foreach ($entity->taxonomy_groups as $group) {
      if ($group->target_id) {
        $groups[] = $group->target_id;
      }
    }

    return $groups;
  }

  /**
   * @todo:
   */
  public function getGroupAncestry($entity) {
    $groups = array();
    if (!empty($entity->taxonomy_groups)) {
      foreach ($entity->taxonomy_groups as $group) {
        if ($group->target_id) {
          $groups[] = $group->target_id;
        }
      }
    }

    return $groups;
  }

  /**
   * Calculate total users in a group.
   *
   */
  public function userCount($tid) {
    $query = $this->connection->select('user__taxonomy_groups', 'utg');
    $query->addExpression('COUNT(DISTINCT utg.entity_id)');
    $query->condition('utg.taxonomy_groups_target_id', $tid);
    $query->addTag('term_user_count');
    return $query->execute()->fetchField();
  }

  /**
   * Calculate total posts in a group.
   *
   */
  public function postCount($tid) {
    $query = $this->connection->select('groups_index', 'gi');
    $query->addExpression('COUNT(DISTINCT gi.nid)');
    $query->condition('gi.tid', $tid);
    $query->addTag('term_node_count');
    return $query->execute()->fetchField();
  }


  /**
   * Get the relationship of the node type with user groups.
   *
   * @param $type
   *
   * @return int
   *   0 = none, 1 = group node, 2 = group entity.
   */
  public function getEntityGroupMode($type) {
    $config = $this->configFactory->get('sug.settings');
    switch (TRUE) {
      case ($config->get('group_node.' . $type) == 1) :
        $mode = 1;
        break;

      case ($config->get('group_entity.' . $type) == 1) :
        $mode = 2;
        break;

      default:
        $mode = 0;
    }
    return $mode;
  }


  /**
   * @todo:
   * Move to group role management.
   *
   * @param $node
   * @param \Drupal\Core\Session\AccountInterface|NULL $account
   * @return bool
   */
  public function hasGroupPermission($node, \Drupal\Core\Session\AccountInterface $account = NULL) {
    // All groups this node was posted to.
    $node_groups = $this->getGroupAncestry($node);

    $account = ($account !== NULL) ? $account : \Drupal::currentUser();
    $user = User::load($account->id());
    // All groups this user is a member of.
    $user_groups = $this->getGroupAncestry($user);
    // User has access if node was posted to at least one group they're member of.
    return (count($node_groups) && count($user_groups) && count(array_intersect($node_groups, $user_groups)));
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $vars = $this->defaultSleep();
    // Do not serialize static cache.
    unset($vars['history'], $vars['index'], $vars['lastPostData'], $vars['groupChildren'], $vars['groupStatistics']);
    return $vars;
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    $this->defaultWakeup();
    // Initialize static cache.
    $this->history = array();
    $this->lastPostData = array();
    $this->groupChildren = array();
    $this->groupStatistics = array();
    $this->index = NULL;
  }

}
