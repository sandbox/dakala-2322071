<?php

/**
 * @file
 * Contains \Drupal\sug\NodePermissions.
 */

namespace Drupal\sug;

use Drupal\Core\Routing\UrlGeneratorTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;

/**
 * Provides dynamic permissions for nodes of different types.
 */
class NodePermissions  {

  use StringTranslationTrait;
  use UrlGeneratorTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The node type permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function groupNodeTypePermissions() {
    $perms = array();
    // Generate node permissions for all node types.
    foreach (NodeType::loadMultiple() as $type) {
      if (\Drupal::service('group.manager')->isGroupNodeType('node', $type->id())) {
        $perms += $this->buildPermissions($type);
      }
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\node\Entity\NodeType $type
   *   The node type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(NodeType $type) {
    $type_id = $type->id();
    $type_params = array('%type_name' => $type->label());

    return array(
      "create $type_id content" => array(
        'title' => $this->t('%type_name: Create new group content', $type_params),
      ),
      "edit own $type_id content" => array(
        'title' => $this->t('%type_name: Edit own group content', $type_params),
      ),
      "edit any $type_id content" => array(
        'title' => $this->t('%type_name: Edit any group content', $type_params),
      ),
      "delete own $type_id content" => array(
        'title' => $this->t('%type_name: Delete own group content', $type_params),
      ),
      "delete any $type_id content" => array(
        'title' => $this->t('%type_name: Delete any group content', $type_params),
      ),
      "view $type_id revisions" => array(
        'title' => $this->t('%type_name: View group content revisions', $type_params),
      ),
      "revert $type_id revisions" => array(
        'title' => $this->t('%type_name: Revert group content revisions', $type_params),
        'description' => t('Group role requires permission <em>view revisions</em> and <em>edit rights</em> for group nodes in question, or <em>administer nodes</em>.'),
      ),
      "delete $type_id revisions" => array(
        'title' => $this->t('%type_name: Delete group content revisions', $type_params),
        'description' => $this->t('Group role requires permission to <em>view revisions</em> and <em>delete rights</em> for group nodes in question, or <em>administer nodes</em>.'),
      ),
    );
  }

}
