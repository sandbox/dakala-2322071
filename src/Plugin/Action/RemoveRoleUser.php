<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\Action\RemoveRoleUser.
 */

namespace Drupal\sug\Plugin\Action;

use Drupal\sug\Plugin\Action\ChangeUserRoleBase;

/**
 * Removes a role from a user.
 *
 * @Action(
 *   id = "user_remove_group_role_action",
 *   label = @Translation("Remove a group role from the selected users"),
 *   type = "user"
 * )
 */
class RemoveRoleUser extends ChangeUserRoleBase {

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    $tid = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $rid = $this->configuration['rid'];

    if (!empty($account->group_roles[$tid]) && in_array($rid, $account->group_roles[$tid])) {
      db_delete('user_group_roles')
        ->condition('uid', $account->id())
        ->condition('tid', $tid)
        ->condition('rid', $rid)
        ->execute();
    }
  }
}
