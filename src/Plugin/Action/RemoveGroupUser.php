<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\Action\RemoveRoleUser.
 */

namespace Drupal\sug\Plugin\Action;

use Drupal\sug\Plugin\Action\ChangeUserGroupBase;

/**
 * Removes a user from a group.
 *
 * @Action(
 *   id = "user_remove_group_action",
 *   label = @Translation("Remove the selected users from a group"),
 *   type = "user"
 * )
 */
class RemoveGroupUser extends ChangeUserGroupBase {

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    $tid = \Drupal::routeMatch()->getParameter('taxonomy_term');
    // Remove any group roles.
    \Drupal::service('group.user_group_role_storage')->removeGroupRoles($account, $tid);

    // Revoke group membership.
    foreach ($account->taxonomy_groups as $delta => $target) {
      if ($tid == $account->taxonomy_groups[$delta]->target_id) {
        unset($account->taxonomy_groups[$delta]);
      }
    }
    $account->save();
  }
}
