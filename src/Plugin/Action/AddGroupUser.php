<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\Action\AddRoleUser.
 */

namespace Drupal\sug\Plugin\Action;


/**
 * Adds a user to a group.
 *
 * @Action(
 *   id = "user_add_group_action",
 *   label = @Translation("Add selected users to a group"),
 *   type = "user"
 * )
 */
class AddGroupUser extends ChangeUserGroupBase {

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    $tid = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $account->taxonomy_groups = ['target_id' => $tid];
    $account->save();
  }

}
