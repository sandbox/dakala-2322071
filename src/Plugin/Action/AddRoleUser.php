<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\Action\AddRoleUser.
 */

namespace Drupal\sug\Plugin\Action;

use Drupal;

/**
 * Adds a group role to a user.
 *
 * @Action(
 *   id = "user_add_group_role_action",
 *   label = @Translation("Add a group role to the selected users"),
 *   type = "user"
 * )
 */
class AddRoleUser extends ChangeUserRoleBase {

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    $tid = \Drupal::routeMatch()->getParameter('taxonomy_term');
    $rid = $this->configuration['rid'];
    if(empty($account->group_roles[$tid]) || ($account->group_roles[$tid] != $rid)) {
      db_insert('user_group_roles')->fields(array('uid' => $account->id(), 'tid' => $tid, 'rid' => $rid))->execute();
    }
  }

}
