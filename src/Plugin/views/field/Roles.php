<?php

/**
 * @file
 * Contains \Drupal\user\Plugin\views\field\Roles.
 */

namespace Drupal\sug\Plugin\views\field;

use Drupal\Core\Database\Connection;
use Drupal\sug\UserGroupRoleStorageInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\field\PrerenderList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to provide a list of roles.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("user_group_roles")
 */
class Roles extends PrerenderList {

  /**
   * Group manager service.
   *
   * @var \Drupal\sug\GroupRoleStorageInterface
   */
  protected $groupRoleStorage;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database Service Object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, UserGroupRoleStorageInterface $group_role_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->database = $database;
    $this->groupRoleStorage = $group_role_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('database'), $container->get('group.user_group_role_storage'));
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->additional_fields['uid'] = array('table' => 'users_field_data', 'field' => 'uid');
  }

  public function query() {
    $this->addAdditionalFields();
    $this->field_alias = $this->aliases['uid'];
  }

  public function preRender(&$values) {
    $uids = array();
    $this->items = array();

    foreach ($values as $result) {
      $uids[] = $this->getValue($result);
    }

    if ($uids) {
      $tid = \Drupal::routeMatch()->getParameter('taxonomy_term');

      $roles = sug_roles(TRUE);
      $result = $this->groupRoleStorage->loadGroupRoles($tid, $uids);
      foreach ($result as $role) {
        $this->items[$role->uid][$role->rid]['role'] = $roles[$role->rid]->label();
        $this->items[$role->uid][$role->rid]['rid'] = $role->rid;
      }

      // Sort the roles for each user by role weight.
      $ordered_roles = array_keys($roles);
      sort($ordered_roles);
      foreach ($this->items as &$user_roles) {
        // Create an array of rids that the user has in the role weight order.
        $sorted_keys  = array_intersect_key($ordered_roles, $user_roles);
        // Merge with the unsorted array of role information which has the
        // effect of sorting it.
        $user_roles = array_merge($sorted_keys, $user_roles);
      }
    }
  }

  function render_item($count, $item) {
    return $item['role'];
  }

  protected function documentSelfTokens(&$tokens) {
    $tokens['{{ ' . $this->options['id'] . '__role' . ' }}'] = $this->t('The name of the group role.');
    $tokens['{{ ' . $this->options['id'] . '__rid' . ' }}'] = $this->t('The role machine-name of the group role.');
  }

  protected function addSelfTokens(&$tokens, $item) {
    if (!empty($item['role'])) {
      $tokens['{{ ' . $this->options['id'] . '__role' . ' }}'] = $item['role'];
      $tokens['{{ ' . $this->options['id'] . '__rid' . ' }}'] = $item['rid'];
    }
  }

}
