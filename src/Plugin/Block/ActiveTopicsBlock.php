<?php

/**
 * @file
 * Contains \Drupal\sug\Plugin\Block\ActiveTopicsBlock.
 */

namespace Drupal\sug\Plugin\Block;

use Drupal\user\Entity\User;

/**
 * Provides an 'My group posts' block.
 *
 * @Block(
 *   id = "group_post_block",
 *   admin_label = @Translation("My group posts"),
 *   category = @Translation("Lists (Views)")
 * )
 */
class ActiveTopicsBlock extends GroupBlockBase {

  /**
   * {@inheritdoc}
   */
  protected function buildGroupQuery() {
    //f.nid, f.created, f.title, sticky, created
    $query = '';
    $account = User::load(\Drupal::currentUser()->id());
    $tids = \Drupal::service('group.manager')->getGroups($account);
    if (count($tids)) {
      $query = db_select('groups_index', 'f')
        ->distinct()
        ->fields('f', ['nid', 'title', 'sticky', 'created'])
        ->condition('f.tid', $tids, 'IN')
        ->addTag('node_access')
        ->addMetaData('base_table', 'groups_index')
        ->orderBy('f.created', 'DESC')
        ->range(0, $this->configuration['block_count']);
    }
    return $query;
  }

}
