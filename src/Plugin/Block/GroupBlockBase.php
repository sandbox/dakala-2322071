<?php

/**
 * @file
 * Contains \Drupal\sug\Plugin\Block\ForumBlockBase.
 */

namespace Drupal\sug\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Provides a base class for Forum blocks.
 */
abstract class GroupBlockBase extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $elements = array();
    $query = $this->buildGroupQuery();
    if ($query && $result = $query->execute()) {
      if ($node_title_list = node_title_list($result)) {
        $elements['group_list'] = $node_title_list;
        $elements['group_more'] = array(
          '#type' => 'more_link',
          '#url' => Url::fromRoute('group.index'),
          '#attributes' => array('title' => $this->t('Read the latest group topics.')),
        );
      }
    }
    return $elements;
  }

  /**
   * Builds the select query to use for this group block.
   *
   * @return \Drupal\Core\Database\Query\Select
   *   A Select object.
   */
  abstract protected function buildGroupQuery();

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'properties' => array(
        'administrative' => TRUE,
      ),
      'block_count' => 10,
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $range = range(2, 20);
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => $this->t('Number of topics'),
      '#default_value' => $this->configuration['block_count'],
      '#options' => array_combine($range, $range),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_count'] = $form_state->getValue('block_count');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user.node_grants:view']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['node_list']);
  }

}
