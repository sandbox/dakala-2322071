<?php

/**
 * @file
 * Contains \Drupal\sug\Plugin\Validation\Constraint\ForumLeafConstraint.
 */

namespace Drupal\sug\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the node is assigned only a "leaf" term in the group taxonomy.
 *
 * @Constraint(
 *   id = "GroupLeaf",
 *   label = @Translation("Group leaf", context = "Validation"),
 * )
 */
class GroupLeafConstraint extends Constraint {

  public $selectGroup = 'Select a group.';
  public $noLeafMessage = 'The item %group is a group container, not a group. Select one of the groups below instead.';
}
