<?php

/**
 * @file
 * Contains \Drupal\sug\Plugin\Validation\Constraint\ForumLeafConstraintValidator.
 */

namespace Drupal\sug\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ForumLeaf constraint.
 */
class GroupLeafConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $item = $items->first();
    if (!isset($item)) {
      return NULL;
    }

    // Verify that a term has been selected.
    if (!$item->entity) {
      $this->context->addViolation($constraint->selectGroup);
    }

    // The group_container flag must not be set.
    if (!empty($item->entity->group_container->value)) {
      $this->context->addViolation($constraint->noLeafMessage, array('%group' => $item->entity->getName()));
    }
  }

}
