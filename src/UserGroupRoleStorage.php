<?php

/**
 * @file
 * Contains \Drupal\user\RoleStorageController.
 */

namespace Drupal\sug;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Database\Connection;
use Drupal\sug\GroupManagerInterface;

/**
 * Controller class for user roles.
 */
class UserGroupRoleStorage implements UserGroupRoleStorageInterface {

  /**
   * Group manager service.
   *
   * @var \Drupal\sug\GroupManagerInterface
   */
  protected $groupManager;

  /**
   * The active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a GroupsIndexStorage object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The current database connection.
   */
  function __construct(Connection $database, GroupManagerInterface $group_manager) {
    $this->groupManager = $group_manager;
    $this->database = $database;
  }

  /**
   * Add the group role to this user.
   *
   * @param $account
   * @param $rid
   * @param $tid
   */
  public function addGroupRole($account, $rid, $tid) {
    $this->database->insert('user_group_roles')
      ->fields(array(
        'uid' => $account->id(),
        'tid' => $tid,
        'rid' => $rid,
      ))->execute();
  }

  /**
   * Remove the group role from this user.
   *
   * @param $account
   * @param $rid
   * @param $tid
   */
  public function removeGroupRole($account, $tid, $rid) {
    $this->database->delete('user_group_roles')
      ->condition('uid', $account->id())
      ->condition('tid', $tid)
      ->condition('rid', $rid)
      ->execute();
  }

  public function removeGroupRoles($account, $tid) {
    $this->database->delete('user_group_roles')
      ->condition('uid', $account->id())
      ->condition('tid', $tid)
      ->execute();
  }

  public function loadUserGroupRoles($uid) {
    $query = $this->database->select('user_group_roles', 'ugr')
      ->fields('ugr', ['tid', 'rid'])
      ->condition('uid', $uid);

    $roles = [];
    foreach ($query->execute()->fetchAll() as $role) {
      $roles[$role->tid][] = $role->rid;
    }
    return $roles;
  }

  /**
   * {@inheritdoc}
   */
  public function loadGroupRoles($tid, array $uids) {
    $query = $this->database->select('user_group_roles', 'ugr')
      ->fields('ugr', ['uid', 'rid'])
      ->condition('tid', $tid)
      ->condition('uid', $uids, 'IN');

    return $query->execute()->fetchAll();
  }


  /**
   * {@inheritdoc}
   */
  public function deleteGroupRoleReferences(array $rids) {
    // Remove the group role from all users.
    db_delete('user_group_roles')
      ->condition('rid', $rids)
      ->execute();
  }



//  public function getGroupRolesForGroup(\Drupal\taxonomy\Entity\Term $term, array $users) {
//    $group_roles = array();
//    $roles = $this->loadGroupRoles($term, $users);
//    foreach ($roles as $role) {
//      $group_roles[$role->machine_name][$role->uid][] = $role->rid;
//    }
//    return $group_roles;
//  }


}
