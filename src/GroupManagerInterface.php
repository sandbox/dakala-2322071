<?php

/**
 * @file
 * Contains \Drupal\sug\GroupManagerInterface.
 */

namespace Drupal\sug;

use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides group manager interface.
 */
interface GroupManagerInterface {

  /**
   * Gets list of group topics.
   *
   * @param int $tid
   *   Term ID.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to fetch topics for.
   *
   * @return array
   *   Array with keys 'topics' and 'header'.
   */
  public function getTopics($tid, AccountInterface $account);

  /**
   * Utility method to fetch the child groups for a given group.
   *
   * @param int $vid
   *   The group vocabulary ID.
   * @param int $tid
   *   The group ID to fetch the children for.
   *
   * @return array
   *   Array of children.
   */
  public function getChildren($vid, $tid);

  /**
   * Generates and returns the group index.
   *
   * The group index is a pseudo term that provides an overview of all groups.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   A pseudo term representing the overview of all groups.
   */
  public function getIndex();

  /**
   * Resets the GroupManager index and history.
   */
  public function resetCache();

  /**
   * Fetches the parent groups for a given group.
   *
   * @param int $tid
   *   Term ID.
   *
   * @return array
   *   Array of parent terms.
   *
   * @deprecated Scheduled to be removed in 9.0.x, see
   *   https://www.drupal.org/node/2371593.
   */
  public function getParents($tid);

  /**
   * Checks whether a node can be used in a group, based on its content type.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node entity.
   *
   * @return bool
   *   Boolean indicating if the node can be assigned to a group.
   */
  public function checkNodeType(EntityInterface $entity);

  /**
   * Calculates the number of new posts in a group that the user has not yet read.
   *
   * Nodes are new if they are newer than HISTORY_READ_LIMIT.
   *
   * @param int $term
   *   The term ID of the group.
   * @param int $uid
   *   The user ID.
   *
   * @return
   *   The number of new posts in the group that have not been read by the user.
   */
  public function unreadTopics($term, $uid);


}
