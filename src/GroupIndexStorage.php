<?php

/**
 * @file
 * Contains \Drupal\sug\GroupsIndexStorage.
 */

namespace Drupal\sug;
use Drupal\Core\Database\Connection;
use Drupal\node\NodeInterface;
use Drupal\sug\GroupManagerInterface;

/**
 * Handles CRUD operations to {groups_index} table.
 */
class GroupIndexStorage implements GroupIndexStorageInterface {

  /**
   * Group manager service.
   *
   * @var \Drupal\sug\GroupManagerInterface
   */
  protected $groupManager;

  /**
   * The active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a GroupsIndexStorage object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The current database connection.
   */
  function __construct(Connection $database, GroupManagerInterface $group_manager) {
    $this->groupManager = $group_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalTermIds(NodeInterface $node) {
    return $this->database->queryRange("SELECT f.tid FROM {groups} f INNER JOIN {node} n ON f.vid = n.vid WHERE n.nid = :nid ORDER BY f.vid DESC", 0, 1, array(':nid' => $node->id()))->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function create(NodeInterface $node) {
    foreach($node->group_tids as $group_tid) {
      $this->database->insert('groups')
        ->fields(array(
          'tid' => $group_tid,
          'vid' => $node->getRevisionId(),
          'nid' => $node->id(),
        ))
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function read(array $vids) {
    return $this->database->select('groups', 'f')
      ->fields('f', array('nid', 'tid'))
      ->condition('f.vid', $vids, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete(NodeInterface $node) {
    $this->database->delete('groups')
      ->condition('nid', $node->id())
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision(NodeInterface $node) {
    $this->database->delete('groups')
      ->condition('nid', $node->id())
      ->condition('vid', $node->getRevisionId())
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function update(NodeInterface $node) {
    foreach ($node->group_tids as $group_tid) {
      $this->database->update('groups')
        ->fields(array('tid' => $group_tid))
        ->condition('vid', $node->getRevisionId())
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createIndex(NodeInterface $node) {
    $query = $this->database->insert('groups_index')
      ->fields(array('nid', 'title', 'tid', 'sticky', 'created'));
    foreach ($node->getTranslationLanguages() as $langcode => $language) {
      $translation = $node->getTranslation($langcode);
      foreach ($translation->taxonomy_groups as $item) {
        $query->values(array(
          'nid' => $node->id(),
          'title' => $translation->label(),
          'tid' => $item->target_id,
          'sticky' => (int) $node->isSticky(),
          'created' => $node->getCreatedTime(),
        ));
      }
    }
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteIndex(NodeInterface $node) {
    $this->database->delete('groups_index')
      ->condition('nid', $node->id())
      ->execute();
  }

}
