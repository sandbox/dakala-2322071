<?php

/**
 * @file
 * Contains \Drupal\sug\Form\Overview.
 */

namespace Drupal\sug\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\taxonomy\Form\OverviewTerms;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\sug\GroupManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * Provides group overview form for the group vocabulary.
 */
class Overview extends OverviewTerms {

  /**
   * Entity manager Service Object.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Group manager service.
   *
   * @var \Drupal\sug\GroupManagerInterface
   */
  protected $groupManager;

  /**
   * Constructs a \Drupal\group\Form\OverviewForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\sug\GroupManagerInterface $group_manager
   *   The group manager service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityManagerInterface $entity_manager, GroupManagerInterface $group_manager) {
    parent::__construct($module_handler, $entity_manager);
    $this->entityManager = $entity_manager;
    $this->groupManager = $group_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity.manager'),
      $container->get('group.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $group_config = $this->config('sug.settings');
    $vid = $group_config->get('vocabulary');
    $vocabulary = $this->entityManager->getStorage('taxonomy_vocabulary')->load($vid);
    if (!$vocabulary) {
      throw new NotFoundHttpException();
    }

    // Build base taxonomy term overview.
    $form = parent::buildForm($form, $form_state, $vocabulary);

    foreach (Element::children($form['terms']) as $key) {
      if (isset($form['terms'][$key]['#term'])) {
        $term = $form['terms'][$key]['#term'];

        // TODO: Update title when saving term?
        // Update title if term has a tag.
        $form['terms'][$key]['term']['#title'] = $this->groupManager->getGroupTagLabel($term);

        $form['terms'][$key]['term']['#url'] = Url::fromRoute('group.page', ['taxonomy_term' => $term->id()]);
        unset($form['terms'][$key]['operations']['#links']['delete']);
        $route_parameters = $form['terms'][$key]['operations']['#links']['edit']['url']->getRouteParameters();
        if (!empty($term->group_container->value)) {
          $form['terms'][$key]['operations']['#links']['edit']['title'] = $this->t('edit container');
          $form['terms'][$key]['operations']['#links']['edit']['url'] = Url::fromRoute('entity.taxonomy_term.group_edit_container_form', $route_parameters);
        }
        else {
          $form['terms'][$key]['operations']['#links']['edit']['title'] = $this->t('edit group');
          $form['terms'][$key]['operations']['#links']['edit']['url'] = Url::fromRoute('entity.taxonomy_term.group_edit_form', $route_parameters);
        }
        // We don't want the redirect from the link so we can redirect the
        // delete action.
        unset($form['terms'][$key]['operations']['#links']['edit']['query']['destination']);
      }
    }

    // Remove the alphabetical reset.
    unset($form['actions']['reset_alphabetical']);

    // Use the existing taxonomy overview submit handler.
    $form['terms']['#empty'] = $this->t('No containers or groups available. <a href=":container">Add container</a> or <a href=":group">Add group</a>.', array(
      ':container' => $this->url('group.add_container'),
      ':group' => $this->url('group.add_group')
    ));
    return $form;
  }

}
