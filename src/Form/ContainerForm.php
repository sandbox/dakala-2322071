<?php

/**
 * @file
 * Contains \Drupal\sug\Form\ContainerForm.
 */

namespace Drupal\sug\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sug\Form\GroupForm;

/**
 * Base form for container term edit forms.
 */
class ContainerForm extends GroupForm {

  /**
   * Reusable url stub to use in watchdog messages.
   *
   * @var string
   */
  protected $urlStub = 'group-container';

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $taxonomy_term = $this->entity;
    // Build the bulk of the form from the parent group form.
    $form = parent::form($form, $form_state, $taxonomy_term);

    // Set the title and description of the name field.
    $form['name']['#title'] = $this->t('Container name');
    $form['name']['#description'] = $this->t('Short but meaningful name for this collection of related groups.');

    // Alternate description for the container parent.
    $form['parent'][0]['#description'] = $this->t('Containers are usually placed at the top (root) level, but may also be placed inside another container or group.');
    $this->groupFormType = $this->t('group container');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $entity = parent::buildEntity($form, $form_state);
    $entity->group_container = TRUE;
    return $entity;
  }

}
