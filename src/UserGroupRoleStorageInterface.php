<?php

/**
 * @file
 * Contains \Drupal\user\RoleStorageControllerInterface.
 */

namespace Drupal\sug;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines a common interface for roel entity controller classes.
 */
interface UserGroupRoleStorageInterface {

  /**
   * Delete role references.
   *
   * @param array $rids
   *   The list of role IDs being deleted. The storage controller should
   *   remove permission and user references to this role.
   */
  public function deleteGroupRoleReferences(array $rids);

  public function loadGroupRoles($tid, array $uids);

  public function addGroupRole($account, $rid, $tid);

  public function removeGroupRole($account, $tid, $rid);

  public function removeGroupRoles($account, $tid);

}
