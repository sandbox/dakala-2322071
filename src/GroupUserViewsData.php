<?php

/**
 * @file
 * Contains \Drupal\user\UserViewsData.
 */

namespace Drupal\sug;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the user entity type.
 */
//class GroupUserViewsData extends EntityViewsData {
//
//  /**
//   * {@inheritdoc}
//   */
//  public function getViewsData() {
//    $data = parent::getViewsData();
////user_group_roles
//    $data['user_group_roles']['table']['group']  = t('Simple user group');
//
//    $data['user_group_roles']['table']['join'] = array(
//      'users_field_data' => array(
//        'left_field' => 'uid',
//        'field' => 'entity_id',
//      ),
//    );
//
//    $data['user_group_roles']['roles_target_id'] = array(
//      'title' => t('Group roles'),
//      'help' => t('Group roles that a user belongs to.'),
//      'field' => array(
//        'id' => 'user_roles',
//        'no group by' => TRUE,
//      ),
//      'filter' => array(
//        'id' => 'user_roles',
//        'allow empty' => TRUE,
//      ),
//      'argument' => array(
//        'id' => 'user__roles_rid',
//        'name table' => 'role',
//        'name field' => 'name',
//        'empty field name' => t('No role'),
//        'zero is null' => TRUE,
//        'numeric' => TRUE,
//      ),
//    );
//
//    return $data;
//  }
//
//}
