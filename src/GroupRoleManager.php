<?php
/**
 * Created by PhpStorm.
 * User: dakala
 * Date: 16/12/2015
 * Time: 09:37
 */

namespace Drupal\sug;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\EntityInterface;


class GroupRoleManager implements GroupRoleManagerInterface {

  /**
   * Group settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity manager service
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Database connection
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs the group manager service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityManagerInterface $entity_manager, Connection $connection, TranslationInterface $string_translation) {
    $this->configFactory = $config_factory;
    $this->entityManager = $entity_manager;
    $this->connection = $connection;
    $this->stringTranslation = $string_translation;
  }

  public function hasGroupPermission($entity, AccountInterface $account = NULL, $permission = '') {
    if (\Drupal::currentUser()->id() == 1) {
      return TRUE;
    }

    // if no permission, only check group membership.
    if (!strlen($permission)) {
      $entity_groups = \Drupal::service('group.manager')->getGroupAncestry($entity);

      $account = (!empty($account)) ? $account : \Drupal::currentUser();
      $user_groups =  \Drupal::service('group.manager')->getGroupAncestry(User::load($account->id()));

      $access = (count($entity_groups) && count($user_groups)) ? array_intersect($entity_groups, $user_groups) : [];

      return count($access);
    }
  }

  public function hasGroupRole($rid) {

  }

  public function getGroupRoles($exclude_locked_roles = FALSE) {
    // TODO: Implement getRoles() method.
  }

}
