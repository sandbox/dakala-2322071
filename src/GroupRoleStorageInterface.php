<?php

/**
 * @file
 * Contains \Drupal\user\RoleStorageControllerInterface.
 */

namespace Drupal\sug;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines a common interface for roel entity controller classes.
 */
interface GroupRoleStorageInterface extends ConfigEntityStorageInterface {

  public function isPermissionInGroupRoles($permission, array $rids);
}
